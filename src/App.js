import React from "react";
import LoginScreen from "./containers/LoginScreen/LoginScreen";
import "./App.css";

function App() {
  return <LoginScreen />;
}

export default App;
