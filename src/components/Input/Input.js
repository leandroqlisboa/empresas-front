/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Row, Col } from "reactstrap";

import "./Input.scss";

const Input = ({ type, icon, placeholder, value }) => {
  return (
    <>
      <Row>
        <Col className="d-flex align-items-center justify-content-center">
          <div className="input__container">
            <img src={icon} />
            <input
              className="input"
              type={type}
              placeholder={placeholder}
              value={value}
            />
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Input;
