import React from "react";
import "./Button.scss";
const Button = ({ id, children, onClick }) => {
  return (
    <button id={id} children={children} className="button" onClick={onClick} />
  );
};

export default Button;
