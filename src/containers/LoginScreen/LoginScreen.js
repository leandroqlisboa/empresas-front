import React from "react";
import { Container, Row, Col } from "reactstrap";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import logoHome from "../../assets/images/logo/logo-home.png";
import iconEmail from "../../assets/images/icons/ic-email.png";
import iconPassword from "../../assets/images/icons/ic-cadeado.png";
import "./LoginScreen.scss";
const LoginScreen = props => {
  return (
    <Container>
      <Row>
        <Col className="d-flex justify-content-center">
          <img
            style={{ marginBottom: "67px", marginTop: "100px" }}
            width={295}
            height={72}
            src={logoHome}
            alt={"logo"}
          />
        </Col>
      </Row>
      <Row className="d-flex flex-column text__container">
        <Col className="d-flex flex-column justify-content-center align-items-center">
          <p>BEM-VINDO AO EMPRESAS</p>
          <p>
            Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
          </p>
        </Col>
      </Row>
      <Row>
        <Col className="d-flex flex-column align-items-center justify-content-center">
          <Input type={"text"} placeholder={"Email"} icon={iconEmail} />
          <Input type={"password"} placeholder={"Senha"} icon={iconPassword} />
          <Button id={"chama"} children={"ENTRAR"} />
        </Col>
      </Row>
    </Container>
  );
};

export default LoginScreen;
